from django.conf.urls import url, include
from django.contrib import admin
from django.urls import path
from reservas.views import reserva
from reservas.views import inicio



urlpatterns = [
    url(r'^sala/(?P<cd_recurso>\d+)/$', reserva.calendario , name='RESERVAS'),
    url(r'^sala/excluir/(?P<cd_reserva>\d+)/$', reserva.excluir, name='EXCLUIR'),
    url(r'^inicio/(?P<cd_recurso>\d+)/$', inicio.inicio, name='INICIO'),
    path('admin/', admin.site.urls),
]
