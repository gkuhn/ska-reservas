# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class Equipe(models.Model):

    class Meta:
        managed = False
        db_table = 'EQUIPE'


class GrupoPermissao(models.Model):
    cd_grupo = models.AutoField(primary_key=True)
    nome = models.CharField(unique=True, max_length=200, blank=True, null=True)
    descricao = models.TextField(blank=True, null=True)  # This field type is a guess.

    class Meta:
        managed = False
        db_table = 'GRUPO_PERMISSAO'


class Localidades(models.Model):
    id_localidade = models.IntegerField(primary_key=True)
    localizacao = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'LOCALIDADES'
        unique_together = (('id_localidade', 'localizacao'),)


class LogReservas(models.Model):

    class Meta:
        managed = False
        db_table = 'LOG_RESERVAS'


class LogSu(models.Model):

    class Meta:
        managed = False
        db_table = 'LOG_SU'


class Objetos(models.Model):
    cd_objeto = models.AutoField(primary_key=True)
    nm_objeto = models.CharField(max_length=255)
    ds_local_origem_obj = models.CharField(max_length=255)
    ds_local_atual_obj = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'OBJETOS'
        unique_together = (('cd_objeto', 'nm_objeto'),)


class Papel(models.Model):

    class Meta:
        managed = False
        db_table = 'PAPEL'


class PapelUsuario(models.Model):
    cd_papel = models.ForeignKey(Papel, models.DO_NOTHING, db_column='cd_papel', blank=True, null=True)
    cd_usuario = models.ForeignKey('Usuario', models.DO_NOTHING, db_column='cd_usuario', blank=True, null=True)
    cd_equipe = models.ForeignKey(Equipe, models.DO_NOTHING, db_column='cd_equipe', blank=True, null=True)
    obs = models.TextField(blank=True, null=True)  # This field type is a guess.
    createdby = models.CharField(db_column='CreatedBy', max_length=200, blank=True, null=True)  # Field name made lowercase.
    createdon = models.DateTimeField(db_column='CreatedOn', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'PAPEL_USUARIO'


class Permissao(models.Model):

    class Meta:
        managed = False
        db_table = 'PERMISSAO'


class Recurso(models.Model):
    cd_recurso = models.AutoField(primary_key=True)
    nm_recurso = models.CharField(max_length=100)
    ds_recurso = models.CharField(max_length=1000, blank=True, null=True)
    cd_filial = models.IntegerField(blank=True, null=True)
    cd_status = models.IntegerField()
    erased = models.IntegerField()
    fl_email = models.BooleanField(blank=True, null=True)
    cd_entidade_filial = models.IntegerField(blank=True, null=True)
    fl_email_temp = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'RECURSO'


class Reserva(models.Model):
    cd_reserva = models.AutoField(primary_key=True)
    ds_reserva = models.CharField(max_length=2000)
    cd_recurso = models.ForeignKey(Recurso, models.DO_NOTHING, db_column='cd_recurso')
    cd_usuario = models.ForeignKey('Usuario', models.DO_NOTHING, db_column='cd_usuario')
    dt_inicio = models.DateTimeField()
    dt_termino = models.DateTimeField(blank=True, null=True)
    erased = models.IntegerField()
    fl_recorrencia = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'RESERVA'


class Responsabilidade(models.Model):
    cd_responsabilidade = models.AutoField(primary_key=True)
    cd_papel = models.ForeignKey(Papel, models.DO_NOTHING, db_column='cd_papel', blank=True, null=True)
    descricao = models.TextField(blank=True, null=True)  # This field type is a guess.
    modifiedby = models.CharField(db_column='ModifiedBy', max_length=200, blank=True, null=True)  # Field name made lowercase.
    modifiedon = models.DateTimeField(db_column='ModifiedOn', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'RESPONSABILIDADE'


class Usuario(models.Model):
    cd_usuario = models.AutoField(primary_key=True)
    nm_nome = models.CharField(unique=True, max_length=200, blank=True, null=True)
    ds_email = models.CharField(max_length=50, blank=True, null=True)
    ds_login = models.CharField(unique=True, max_length=50)
    ds_senha = models.CharField(max_length=32, blank=True, null=True)
    cd_status = models.IntegerField(blank=True, null=True)
    erased = models.IntegerField()
    dt_expiracao = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'USUARIO'


class UsuarioGrupo(models.Model):
