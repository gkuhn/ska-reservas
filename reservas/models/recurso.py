# -*- coding: UTF-8 -*-

from django.contrib.auth.models import User
from django.db import models

#model utilizado para salvar os dados do usuario temporariamente até que ele tenha terminado seu cadastro
#esse modelo também guarda o hash para recuperar a senha
#e é utilizado no login pata checar se precisar completar algum dado
############################################################################################################
############################################################################################################
class Recurso(models.Model):
    cd_recurso = models.AutoField(primary_key=True)
    nm_recurso = models.CharField(max_length=100)
    ds_recurso = models.CharField(max_length=1000, blank=True, null=True)
    cd_filial = models.IntegerField(blank=True, null=True)
    cd_status = models.IntegerField()
    erased = models.IntegerField()
    fl_email = models.BooleanField(blank=True, null=True)
    cd_entidade_filial = models.IntegerField(blank=True, null=True)
    fl_email_temp = models.IntegerField(blank=True, null=True)

############################################################################################################
############################################################################################################

    class Meta:
        managed = False
        db_table = 'RECURSO'

############################################################################################################
############################################################################################################

    def __str__(self): # defini como o resultado aparece com o search (aparece primeiro o nome do recurso)

        return self.nm_recurso

############################################################################################################
############################################################################################################