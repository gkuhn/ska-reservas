# -*- coding: UTF-8 -*-

from django.contrib.auth.models import User
from django.db import models

class UsuarioManager(models.Manager):
    def get_by_natural_key(self, ds_login):
        return self.get(ds_login=ds_login)

############################################################################################################
############################################################################################################
class Usuario(models.Model):
    objects = UsuarioManager()

    cd_usuario = models.AutoField(primary_key=True)
    nm_nome = models.CharField(unique=True, max_length=200, blank=True, null=True)
    ds_email = models.CharField(max_length=50, blank=True, null=True)
    ds_login = models.CharField(unique=True, max_length=50)
    ds_senha = models.CharField(max_length=32, blank=True, null=True)
    cd_status = models.IntegerField(blank=True, null=True)
    erased = models.IntegerField()
    dt_expiracao = models.DateTimeField(blank=True, null=True)

############################################################################################################
############################################################################################################

    class Meta:
        managed = False
        db_table = 'USUARIO'
        unique_together = (('ds_login'),)

############################################################################################################
############################################################################################################

    def __str__(self): # defini como o resultado aparece com o search (aparece primeiro o login)

        return self.ds_login

############################################################################################################
############################################################################################################

    def usuario_login(self):

        return self.ds_login

############################################################################################################
############################################################################################################