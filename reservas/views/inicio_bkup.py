# -*- coding: UTF-8 -*-
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.http import HttpResponse
from django.shortcuts import render
import datetime as dt
from reservas.forms.inicio import InicioForm
from reservas.models.recurso import Recurso
from reservas.models.reserva import Reserva



def inicio(request, cd_recurso=0):

    if request.method == "POST":

        inicioForm = InicioForm(request.POST, request=request) #passando os dados da tela para dentro do form #request=request --> passando a http request pra poder pegar o id da sala pela url)

        if inicioForm.is_valid(): #verificando se os dados que entraram no campo são validos de acordo com o methodo clen (esta lá no form)

            pass

        else:

            return render(request, "index.html")


    else: #se não for post é get

        recurso = Recurso.objects.get(cd_recurso=cd_recurso)

        #verifica se há reserva agora
        now = dt.datetime.today()
        now_plus = dt.datetime.today() + dt.timedelta(seconds=10)

        during_conflict = Reserva.objects.filter(
            cd_recurso=cd_recurso,
            erased=0,
            dt_inicio__lte=now,
            dt_termino__gte=now_plus)

        #se há reserva agora, verifica horário de término da mesma
        if (during_conflict):
            during_conflict = Reserva.objects.get(
                cd_recurso=cd_recurso,
                erased=0,
                dt_inicio__lte=now,
                dt_termino__gte=now_plus)

            final = during_conflict.dt_termino
            final_plus = final + dt.timedelta(minutes=1)

            #verifica se tem reserva "colada" a reserva de agora
            try:
                next_reserva = Reserva.objects.get(
                    cd_recurso=cd_recurso,
                    erased=0,
                    dt_inicio__lte=final_plus,
                    dt_inicio__gte=final)

                final = next_reserva.dt_termino
                final_plus = final + dt.timedelta(minutes=1)

                #laço para próximas reservas "coladas"
                while(next_reserva):
                    next_reserva = Reserva.objects.get(
                        cd_recurso=cd_recurso,
                        erased=0,
                        dt_inicio__lte=final_plus,
                        dt_inicio__gte=final)

                    final = next_reserva.dt_termino
                    final_plus = final + dt.timedelta(minutes=1)

                final = final.time().__format__("%H:%M")
                final = str(final)

            except:
                #guarda tempo final para mandar pra tela
                final = final.time().__format__("%H:%M")
                final = str(final)

            messages.error(request, "Ocupada. Volte mais tarde. \n \n Próximo horário disponível: " + final +"h")

        else:
            # verifica próximo horário disponível
            now = dt.datetime.today()
            plusMin = 10
            # define último horário a buscar (se não, entra em looop - 20h)
            endTime = 20
            cont_time = now.time().__format__('%H')
            cont_time = int(cont_time)

            while cont_time < endTime:
                increment_now = dt.datetime.today() + dt.timedelta(minutes=plusMin)
                cont_time = increment_now.time().__format__('%H')
                cont_time = int(cont_time)
                start_conflict = Reserva.objects.filter(
                    cd_recurso=cd_recurso,
                    erased=0,
                    dt_inicio__range=(now, increment_now))

                if not start_conflict:
                    plusMin = plusMin + 10
                else:
                    cont_time = endTime

            try:
                start_conflict = Reserva.objects.get(
                    cd_recurso=cd_recurso,
                    erased=0,
                    dt_inicio__range=(now, increment_now))

                hrInicio = start_conflict.dt_inicio
                hrInicio = hrInicio.time().__format__('%H:%M')
                hrInicio = str(hrInicio)

            except:
                hrInicio = 0

            if hrInicio == 0:
                messages.success(request, "Livre. \n \n Recurso disponível durante o resto do dia")
            else:
                messages.success(request, "Livre. \n \n Recurso disponível até as " + hrInicio + "h")

        return render(request, "index.html", {'recurso': recurso, 'cd_recurso': cd_recurso})

############################################################################################################
############################################################################################################
