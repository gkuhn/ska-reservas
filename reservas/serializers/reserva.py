from django.utils.formats import number_format
from rest_framework import serializers

from reservas.models.reserva import Reserva


class ReservaSerializer(serializers.ModelSerializer):

    cd_usuario = serializers.CharField(source='usuario_nome')
    cd_recurso = serializers.CharField(source='recurso_nome')
    ds_login = serializers.CharField(source='descricao_login')

    class Meta:
        model = Reserva
        fields = '__all__'
