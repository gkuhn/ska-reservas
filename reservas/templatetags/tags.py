from django import template

register = template.Library()


@register.filter
def sortCodigoStatus(value):
    valoresOrdenados = value.order_by('-ativo')

    return valoresOrdenados


@register.filter
def sortCategoria(value):
    valoresOrdenados = sorted(value, key=lambda x: (x[1]['produtoDetalhes']['categoria']))
    return valoresOrdenados


@register.filter
def addcss(field, css):
    return field.as_widget(attrs={"class": css})


@register.simple_tag
def calculaproduto(lucro, custos, precomaterias):
    preco = round((custos + precomaterias) * (1 + lucro / 100), 2)

    return "R$ " + str(preco)


@register.filter
def adderror(field):
    if field.errors:
        return '<span class="help-block">' + field.errors[0] + '</span>'


@register.filter
def next(some_list, current_index):
    """
    Returns the next element of the list using the current index if it exists.
    Otherwise returns an empty string.
    """
    try:
        return some_list[int(current_index) + 1]  # access the next element
    except:
        return ''  # return empty string in case of exception
