$(function() {
        var availableTags = [
              "melancia.ska",
              "batata.ska",
              "Asp",
              "BASIC",
              "C",
              "C++",
              "Clojure",
              "COBOL",
              "ColdFusion",
              "Erlang",
              "Fortran",
              "Groovy",
              "Haskell",
              "Java",
              "JavaScript",
              "Lisp",
              "Perl",
              "PHP",
              "Python",
              "Ruby",
              "Scala",
              "Scheme"
            ];
            $( ".addresspicker" ).autocomplete({
              source: availableTags
            });

        $( ".addresspicker" ).autocomplete( "option", "appendTo", ".eventInsForm" );        });