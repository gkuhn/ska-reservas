# -*- coding: UTF-8 -*-

from django import forms
from django.contrib.auth.models import User
import datetime
import datetime as dt
from reservas.models.recurso import Recurso
from reservas.models.reserva import Reserva
from reservas.models.usuario import Usuario

#form de cadastro inicial
#VALUES = [cd_reserva,ds_reserva,cd_recurso,cd_usuario,dt_inicio,dt_termino,erased,fl_recorrencia]

class CadastroReservaForm(forms.Form):

    def __init__(self, *args, **kwargs):

        self.request = kwargs.pop('request') #pegando o paramentro request que está lá na view e pasando pra dentro do form

        super(CadastroReservaForm, self).__init__(*args, **kwargs) #sei la porque +- tem uma metodo init na Class ReservaForm, então tenho que chamar no final pra ele se lembrar que tem que terminar de fazer o resto

############################################################################################################

    data = forms.CharField(widget=forms.DateInput(attrs={'id' :'datepicker', 'autocomplete': 'off'}))

############################################################################################################
    horaInicial = forms.CharField(widget=forms.TimeInput(attrs={'autocomplete': 'off'}))

############################################################################################################

    horaFinal = forms.CharField(widget=forms.TimeInput(attrs={'autocomplete': 'off'}))

############################################################################################################

    usuario = forms.CharField(widget=forms.TextInput())

############################################################################################################

    senha = forms.CharField(widget=forms.PasswordInput())

############################################################################################################
############################################################################################################

    def clean(self): #valida os dados que entraram no form

        cleaned_data = self.cleaned_data

        #validação usuário
        if not Usuario.objects.filter(ds_login=cleaned_data['usuario']).exists():
            raise forms.ValidationError('Usuario não existe')
        else:
            if not Usuario.objects.filter(ds_login=cleaned_data['usuario'],
                                          ds_senha=cleaned_data['senha']):
                raise forms.ValidationError('Senha incorreta')

        #recebe e formata dia atual
        hoje = datetime.datetime.today().date()
        hoje_30 = datetime.datetime.today() + dt.timedelta(days=30)
        hoje_30 = hoje_30.date()

        now_less = dt.datetime.today() - dt.timedelta(minutes=2)
        now_less = now_less.time()

        #get data
        data = cleaned_data['data']

        #removendo "/" da estring data
        newdata = data.replace("/", "")

        #tranformando a string data em dateObject
        objectdata = datetime.datetime.strptime(newdata, "%d%m%Y").date()

        #valida se data é maior ou igual a hoje
        if not objectdata >= hoje:
            raise forms.ValidationError('A data selecionada não pode ser inferior a data de hoje')

        if not objectdata <= hoje_30:
            raise forms.ValidationError('A data selecionada não pode ser superioir a 30 dias')

        #pega o cd_recurso que está no http request(na url)
        recurso = Recurso.objects.filter(cd_recurso=self.request.resolver_match.kwargs.get('cd_recurso'))

        #pega hora inicial e final (tryCatch verifica se o formato da entrada é válido)
        try:
            horaInicial = datetime.datetime.strptime(cleaned_data['horaInicial'], '%H:%M').time()
            horaFinal = datetime.datetime.strptime(cleaned_data['horaFinal'], '%H:%M').time()
        except:
            raise forms.ValidationError('Formato de hora inválido')


        #valida se hora final é maior ou igual que a hora inicial
        if horaFinal == horaInicial:
            raise forms.ValidationError('A hora final da reserva não pode ser igual a hora inicial')

        if not horaFinal > horaInicial:
            raise forms.ValidationError('A hora final da reserva não pode ser inferior a hora inicial')

        #if objectdata == hoje:
            #if not horaInicial > now_less:
             #   raise forms.ValidationError('A hora inicial da reserva não pode ser inferior a hora atual')

        #Criando datetime inicial e final - add/remove 1 segundo na hora inicial/final para poder realizar agendamentos sequentes
        timeInicial = dt.datetime.combine(objectdata, horaInicial) + dt.timedelta(seconds = 1)
        timeFinal = dt.datetime.combine(objectdata, horaFinal) - dt.timedelta(seconds = 1)


        # Checando a sala e as colisões
        # https://docs.djangoproject.com/en/dev/ref/models/querysets/#s-range
        start_conflict = Reserva.objects.filter(
            cd_recurso=self.request.resolver_match.kwargs.get('cd_recurso'),
            erased=0,
            dt_inicio__range=(timeInicial, timeFinal))

        end_conflict = Reserva.objects.filter(
            cd_recurso=self.request.resolver_match.kwargs.get('cd_recurso'),
            erased=0,
            dt_termino__range=(timeInicial,timeFinal))

        during_conflict = Reserva.objects.filter(
            cd_recurso=self.request.resolver_match.kwargs.get('cd_recurso'),
            erased=0,
            dt_inicio__lte=timeInicial, dt_termino__gte=timeFinal)

        if (start_conflict or end_conflict or during_conflict):
            booking = 'NOOOOOOOOOOOOOOOOOOOOOO!'
            raise forms.ValidationError('Agendamento não realizado. Horário não disponível.')

        else:
            booking ='Yes!!!!!!!'


        return cleaned_data

############################################################################################################
############################################################################################################
