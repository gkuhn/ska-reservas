# -*- coding: UTF-8 -*-
from django.contrib import messages
from django.shortcuts import render, redirect
import datetime as dt
from reservas.forms.cadastroreserva import CadastroReservaForm
from reservas.forms.excluir import ExcluirReservaForm
from reservas.models.recurso import Recurso
from reservas.models.usuario import Usuario
from reservas.models.reserva import Reserva
from reservas.models.logsu import LogSu
from reservas.serializers.reserva import ReservaSerializer
from reservas.serializers.usuarios import UsuarioSerializer



def calendario(request, cd_recurso=0):

    #Filtra e armazena(serializers) as reserva do recurso no intervalo entre hoje e (hoje + 30 dias)
    reservas = Reserva.objects.filter(cd_recurso=cd_recurso,
                                      erased=0,
                                      dt_inicio__date__range=(dt.datetime.today(), dt.datetime.today() + dt.timedelta(days=30)))
    reservas = ReservaSerializer(reservas, many=True).data

    #Filtra e armazena(serializers) usuarios ativos
    usuarios = Usuario.objects.filter(erased=1, cd_status=0)
    usuarios = UsuarioSerializer(usuarios, many=True).data

    #Pega nome do recurso
    recurso = Recurso.objects.get(cd_recurso=cd_recurso)

    if request.method == "POST":

        # passando os dados da tela para dentro do form #request=request --> passando a http request pra poder pegar o id da sala pela url)
        cadastroReservaForm = CadastroReservaForm(request.POST, request=request)

        # verificando se os dados que entraram no campo são validos de acordo com o methodo clen (esta lá no form)
        if cadastroReservaForm.is_valid():

            # pega data digitada
            data = cadastroReservaForm.cleaned_data['data']

            # removendo "/" da estring data
            newdata = data.replace("/", "")

            # tranformando a string data em date object
            objectdata = dt.datetime.strptime(newdata, "%d%m%Y").date()

            # pega hora inicial e final
            horaInicial = dt.datetime.strptime(cadastroReservaForm.cleaned_data['horaInicial'], '%H:%M').time()
            horaFinal = dt.datetime.strptime(cadastroReservaForm.cleaned_data['horaFinal'], '%H:%M').time()

            # Criando datetime inicial e final
            timeInicial = dt.datetime.combine(objectdata, horaInicial)
            timeFinal = dt.datetime.combine(objectdata, horaFinal)

            reserva = Reserva(ds_reserva='APP Agendamento',
                              cd_recurso=Recurso.objects.get(cd_recurso=cd_recurso),
                              cd_usuario=Usuario.objects.get(ds_login=cadastroReservaForm.cleaned_data['usuario']),
                              dt_inicio=timeInicial,
                              dt_termino=timeFinal,
                              erased=0
                              )
            try:
                reserva.save()
            except:
                messages.error(request, "O sistema falhou em relizar o agendamento, tente novamente.")
                return render(request, "calendar.html", {'cadastroReservaForm': CadastroReservaForm(request=request),
                                                         'excluirReservaForm': ExcluirReservaForm(request=request),
                                                         'reservas': reservas,
                                                         'recurso': recurso,
                                                         'usuarios': usuarios,
                                                         'cd_recurso': cd_recurso,
                                                         })
            try:
                # Salva Log do sistema
                logsu = LogSu(cd_usuario=17456,
                              dt_log=dt.datetime.today(),
                              cd_tipo=0
                              )
                logsu.save()
            except:
                pass

            # Filtra e armazena(serializers) as reserva do recurso no intervalo entre hoje e (hoje + 30 dias)
            reservas = Reserva.objects.filter(cd_recurso=cd_recurso,
                                              erased=0,
                                              dt_inicio__date__range=(
                                              dt.datetime.today(), dt.datetime.today() + dt.timedelta(days=30)))
            reservas = ReservaSerializer(reservas, many=True).data

            messages.success(request, "Agendamento realizado com sucesso! " + str(reserva.cd_recurso))
            return render(request, "calendar.html", {'cadastroReservaForm': CadastroReservaForm(request=request),
                                                     'excluirReservaForm': ExcluirReservaForm(request=request),
                                                     'reservas': reservas,
                                                     'recurso':recurso,
                                                     'usuarios': usuarios,
                                                     'cd_recurso': cd_recurso,
                                                     })

        else:
            messages.error(request, cadastroReservaForm.errors['__all__'])
            return render(request, "calendar.html", {'cadastroReservaForm': CadastroReservaForm(request=request),
                                                     'excluirReservaForm': ExcluirReservaForm(request=request),
                                                     'reservas': reservas,
                                                     'recurso':recurso,
                                                     'usuarios': usuarios,
                                                     'cd_recurso': cd_recurso,
                                                     })

    else:
        return render(request, "calendar.html", {'cadastroReservaForm': CadastroReservaForm(request=request),
                                                 'excluirReservaForm': ExcluirReservaForm(request=request),
                                                 'reservas': reservas,
                                                 'recurso': recurso,
                                                 'usuarios': usuarios,
                                                 'cd_recurso': cd_recurso,
                                                 })


############################################################################################################
############################################################################################################

def excluir(request, cd_reserva=0, cd_recurso=0):

    if request.method == "POST":

        excluirReservaForm = ExcluirReservaForm(request.POST, request=request)

        #guarda codigo do recurso e da código da reserva pra redirecionar no final e enviar para tela
        reserva = Reserva.objects.get(cd_reserva=cd_reserva)
        codigo_recurso = reserva.cd_recurso.cd_recurso

        if excluirReservaForm.is_valid():

            try:
                #Faz a "exclusão virtual" - Seta erased para 1
                Reserva.objects.filter(cd_reserva=cd_reserva).update(erased=1)
            except:
                messages.error(request, "O sistema falhou em excluir o agendamento, tente novamente.")
                return redirect('RESERVAS', cd_recurso=codigo_recurso)

            try:
                #Salva Log do sistema
                logsu = LogSu(cd_usuario=17456,
                              dt_log=dt.datetime.today(),
                              cd_tipo=2
                              )
                logsu.save()
            except:
                pass

            messages.success(request, "Reserva excluida com sucesso")
            return redirect('RESERVAS', cd_recurso=codigo_recurso)

        else:
            messages.error(request, excluirReservaForm.errors['__all__']) #__all__ pq msg é geral é não direto no form field
            return redirect('RESERVAS', cd_recurso=codigo_recurso)

    else:
        obj_reserva = Reserva.objects.get(cd_reserva=cd_reserva)
        return redirect('RESERVAS', cd_recurso=obj_reserva.cd_recurso)
############################################################################################################
############################################################################################################
