# -*- coding: UTF-8 -*-
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.http import HttpResponse
from django.shortcuts import render
import datetime as dt
from reservas.forms.inicio import InicioForm
from reservas.models.recurso import Recurso
from reservas.models.reserva import Reserva



def inicio(request, cd_recurso=0):

    if request.method == "POST":

        inicioForm = InicioForm(request.POST, request=request) #passando os dados da tela para dentro do form #request=request --> passando a http request pra poder pegar o id da sala pela url)

        if inicioForm.is_valid(): #verificando se os dados que entraram no campo são validos de acordo com o methodo clen (esta lá no form)

            pass

        else:

            return render(request, "index.html")


    else: #se não for post é get

        recurso = Recurso.objects.get(cd_recurso=cd_recurso)

        #verifica se há reserva agora
        now = dt.datetime.today()
        now_plus = dt.datetime.today() + dt.timedelta(seconds=10)

        during_conflict = Reserva.objects.filter(
            cd_recurso=cd_recurso,
            erased=0,
            dt_inicio__lte=now,
            dt_termino__gte=now_plus)

        #se há reserva agora, verifica horário de término da mesma
        if (during_conflict):
            during_conflict = Reserva.objects.get(
                cd_recurso=cd_recurso,
                erased=0,
                dt_inicio__lte=now,
                dt_termino__gte=now_plus)

            final = during_conflict.dt_termino
            final_plus = final + dt.timedelta(minutes=1)

            #verifica se tem reserva "colada" a reserva de agora
            try:
                next_reserva = Reserva.objects.get(
                    cd_recurso=cd_recurso,
                    erased=0,
                    dt_inicio__lte=final_plus,
                    dt_inicio__gte=final)

                final = next_reserva.dt_termino
                final_plus = final + dt.timedelta(minutes=1)

                #laço para próximas reservas "coladas"
                while(next_reserva):
                    next_reserva = Reserva.objects.get(
                        cd_recurso=cd_recurso,
                        erased=0,
                        dt_inicio__lte=final_plus,
                        dt_inicio__gte=final)

                    final = next_reserva.dt_termino
                    final_plus = final + dt.timedelta(minutes=1)

                final = final.time().__format__("%H:%M")
                final = str(final)

            except:
                #guarda tempo final para mandar pra tela
                final = final.time().__format__("%H:%M")
                final = str(final)

            messages.error(request, "Ocupada. Volte mais tarde. \n \n Próximo horário disponível: " + final +"h")

        else:

            reservas_ativas = Reserva.objects.filter(cd_recurso=cd_recurso,
                                                     erased=0,
                                                     dt_inicio__date__range=(dt.datetime.today(), dt.datetime.today() + dt.timedelta(days=30)))
            try:
                proxima_reserva = reservas_ativas.order_by('dt_inicio')[0]
                hrInicio = proxima_reserva.dt_inicio.time().__format__('%H:%M')
                dateInicio = proxima_reserva.dt_inicio.date()
                tomorrow = dt.datetime.today().date() + dt.timedelta(days=1)

            except:
                hrInicio = 0


            if hrInicio == 0:
                messages.success(request, "Livre. \n \n Recurso disponível durante os próximos 30 dias")
            else:
                if dateInicio > now.date():
                    if dateInicio == tomorrow:
                        messages.success(request, "Livre "
                                                  "\n \n O recurso está disponível durante o resto do dia. "
                                                  "\n \n Próxima reserva: Amanhã às " + hrInicio + "h")
                    else:
                        messages.success(request, "Livre "
                                                  "\n \n Recurso disponível durante o resto do dia. "
                                                  "\n \n Próxima reserva: Dia " + dateInicio.__format__('%d/%m/%y') + " às " + hrInicio + "h" )
                else:
                    messages.success(request, "Livre "
                                              "\n \n Recurso disponível até às " + hrInicio + "h")

        return render(request, "index.html", {'recurso': recurso, 'cd_recurso': cd_recurso})

############################################################################################################
############################################################################################################

