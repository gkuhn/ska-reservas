//SCRIPT CALENDAR - Gabriela Kuhn

 $(document).ready(function() {
        var cont = 0;

            $('#calendar').fullCalendar({
                height: 750,
                hiddenDays: [0],
                selectable: true,
                editable: true,
                defaultView: 'agendaDay', // Seta qual caledario aparece no início (Mês, semana, dia )
                header: {
                    left: 'prev,next',
                    center: 'title',
                    right: 'agendaWeek,agendaDay'
                },
                allDaySlot: false, // Remove all day events do topo da tabela
                defaultDate: moment(), // Pega current Data
                navLinks: true, // can click day/week names to navigate views
                editable: true,
                locale: 'pt-br',
                eventLimit: true, // allow "more" link when too many events
                eventStartEditable: false, // Não permiti editar eventos por arrastamento
                eventDurationEditable: false, // Não permiti editar eventos por meio de redimensionamento
                minTime: "08:00:00", // Definindo tempo inicial e final do calendario
                maxTime: "19:00:00",
                validRange: { // Range de dias a mostrar - setado para 30 dias(mudar no django também)
                    start: moment().subtract(2, 'days'),
                    end: moment().add(30, 'days')
                },
                viewRender: function(view) {
                    // Disabilitando botões dia anterior
                    if(moment().isAfter(view.intervalStart, 'day')) {
                        $('.fc-prev-button').addClass('fc-state-disabled');
                    } else {
                        $('.fc-prev-button').removeClass('fc-state-disabled');
                    }
                    $('body').remove('.fc-listWeek-button');
                },
                            /*DESABILITADO 21/12/2018 - Função para pegar informações no espaço em branco para adicionar agendamentos
                select: function(start, end, allDay) {
                    //alert('clicked ' + date.format('DD-MM-YYYY') + date.format('HH:mm'));
                    $('#reservaModal').modal();// abre modal
                    var data = start.format('DD-MM-YYYY');// pega data clicada
                    var hora = start.format('HH:mm');// pega hora clicada
                    var endTime = moment(end).format("HH:mm");// pega hora clicada e adiciona + 30 minutos;
                    $(".modal-body #data-clicada").val( data );// coloca data atual no input
                    $(".modal-body #hora-clicada").val( hora );// coloca data atual no input
                    $(".modal-body #banana").val( endTime );// coloca data atual no input
                },*/
                eventClick: function(event, jsEvent, view) { // Abre modal com as informações do agendamento
                    $('#modalTitle').html(event.title);
                    $('#modalBody').html(
                        document.getElementById("id_cd_reserva").value = event.code,
                        document.getElementById("id_responsavel").value = event.title,
                        document.getElementById("id_recurso").value = event.resource,
                        document.getElementById("id_data").value = event.start.format('DD-MM-YYYY'),
                        document.getElementById("id_horaInicial").value = event.start.format('HH:mm'),
                        document.getElementById("id_horaFinal").value = event.end.format('HH:mm'),
                    );
                    $('#eventUrl').attr('href', event.url);
                    $('#fullCalModal').modal();
                    return false;
                },
                //Agendamentos realizados {{ cadastroReservaForm.data|addcss:'form-control' }}
                events: [
                    {% for item in reservas %}
                        {
                            code: '{{ item.cd_reserva }}',
                            title: '{{ item.cd_usuario }}',
                            resource: '{{ item.cd_recurso }}',
                            start: '{{ item.dt_inicio }}',
                            end: '{{ item.dt_termino }}',
                            description: "{{ item.ds_reserva }}",
                            url: "../../excluir/{{ item.cd_reserva }}/"
                    },
                    {% endfor %}]
                });

              });


    

