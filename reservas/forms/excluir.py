# -*- coding: UTF-8 -*-

from django import forms
from django.contrib.auth.models import User
from reservas.models.reserva import Reserva
from reservas.models.usuario import Usuario


class ExcluirReservaForm(forms.Form):

    def __init__(self, *args, **kwargs):

        self.request = kwargs.pop('request')  # pegando o paramentro request que está lá na view e pasando pra dentro do form

        super(ExcluirReservaForm, self).__init__(*args, **kwargs)  # sei la porque +- tem uma metodo init na Class ReservaForm, então tenho que chamar no final pra ele se lembrar que tem que terminar de fazer o resto

############################################################################################################

    reserva = forms.CharField(widget=forms.TextInput(attrs={'readonly':'readonly'}))

############################################################################################################

    usuario = forms.CharField(widget=forms.TextInput())

############################################################################################################

    senha = forms.CharField(widget=forms.PasswordInput())

############################################################################################################
############################################################################################################

    def clean(self):  # valida os dados que entraram no form

        cleaned_data = self.cleaned_data

        cd_reserva = cleaned_data['reserva']

        usuario = cleaned_data['usuario']

        #valida a reserva existe
        try:
            obj_reserva = Reserva.objects.get(cd_reserva=cd_reserva)

        except:
            raise forms.ValidationError('A reserva não existe')


        #valida se usuario é o dono da reserva
        donoReserva = obj_reserva.cd_usuario.ds_login.replace(" ", "")

        if not donoReserva == usuario:
            raise forms.ValidationError('Você não pode realizar esta exclusão pois não é o responsavel da reserva')


        #valida usuario e senha
        if not Usuario.objects.filter(ds_login=cleaned_data['usuario']).exists():
            raise forms.ValidationError('Usuario não existe')
        else:
            if not Usuario.objects.filter(ds_login=cleaned_data['usuario'],
                                          ds_senha=cleaned_data['senha']):
                raise forms.ValidationError('Senha incorreta')

        return cleaned_data

############################################################################################################
############################################################################################################

