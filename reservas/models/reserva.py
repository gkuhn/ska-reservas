# -*- coding: UTF-8 -*-

from django.contrib.auth.models import User
from django.db import models
from reservas.models.recurso import Recurso
from reservas.models.usuario import Usuario

############################################################################################################
############################################################################################################
class Reserva(models.Model):
    cd_reserva = models.AutoField(primary_key=True)
    ds_reserva = models.CharField(max_length=2000)
    cd_recurso = models.ForeignKey(Recurso, models.DO_NOTHING, db_column='cd_recurso')
    cd_usuario = models.ForeignKey('Usuario', models.DO_NOTHING, db_column='cd_usuario')
    dt_inicio = models.DateTimeField()
    dt_termino = models.DateTimeField(blank=True, null=True)
    erased = models.IntegerField()
    fl_recorrencia = models.IntegerField(blank=True, null=True)

###########################################################################################################
############################################################################################################

    class Meta:
        managed = False
        db_table = 'RESERVA'


############################################################################################################
############################################################################################################

    def __str__(self): # defini como o resultado aparece com o search (aparece primeiro o cd usuario)

        return self.ds_reserva

############################################################################################################
############################################################################################################

    def usuario_nome(self):

        return self.cd_usuario.nm_nome

############################################################################################################
############################################################################################################

    def recurso_nome(self):

        return self.cd_recurso.nm_recurso

############################################################################################################
############################################################################################################

    def descricao_login(self):

        return self.cd_usuario.ds_login
