from django.utils.formats import number_format
from rest_framework import serializers

from reservas.models.usuario import Usuario


class UsuarioSerializer(serializers.ModelSerializer):

    cd_usuario = serializers.CharField(source='usuario_login')

    class Meta:
        model = Usuario
        fields = '__all__'
