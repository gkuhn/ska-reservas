# -*- coding: UTF-8 -*-

from django import forms
from django.contrib.auth.models import User
import datetime
import datetime as dt
from reservas.models.recurso import Recurso
from reservas.models.reserva import Reserva
from reservas.models.usuario import Usuario

class InicioForm(forms.Form):

############################################################################################################

    horaInicial = forms.CharField(widget=forms.TextInput())

############################################################################################################

    horaFinal = forms.CharField(widget=forms.TimeInput())

############################################################################################################
############################################################################################################

    def clean(self): #valida os dados que entraram no form

        cleaned_data = self.cleaned_data


        return cleaned_data

############################################################################################################
############################################################################################################
