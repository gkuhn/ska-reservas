# -*- coding: UTF-8 -*-

from django.db import models

############################################################################################################
############################################################################################################
class LogSu(models.Model):

    cd_log = models.IntegerField(primary_key=True)
    cd_usuario = models.IntegerField(blank=True, null=True)
    dt_log = models.DateTimeField(blank=True, null=True)
    cd_tipo = models.IntegerField(blank=True, null=True)
    cd_chave = models.IntegerField(blank=True, null=True)
    cd_classe = models.IntegerField(blank=True, null=True)
    fl_mobile = models.IntegerField(blank=True, null=True)

###########################################################################################################
############################################################################################################

    class Meta:
        managed = True
        db_table = 'LOG_SU'


############################################################################################################
############################################################################################################

    def __unicode__(self):

        return self.cd_log
############################################################################################################
############################################################################################################


    def save(self):
        # Pega o útimo valor de cd_log do banco e incrementa antes de salvar
        # Método necessário pois a coluna cd_log foi criada como não auto-incrementável no banco
        top = LogSu.objects.order_by('-cd_log')[0]
        self.cd_log = top.cd_log + 1
        super(LogSu, self).save()
