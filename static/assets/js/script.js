//Modal - Realizar Agendamento*/

    // Função para realizar agendar através do botão plus
    $(document).on("click", ".plus", function () {

        // :: Abre modal ::
        $('#reserva-currentDate').modal();

        // :: Arredondamento da hora ::
        var data = moment();// data atual
        remainder5 = 5 - (moment().minute() % 5);//arredonamento da hora atual. De 5 em 5 min
        remainder30 = 30 - (moment().minute() % 30);//arredonamento da hora atual. De 30 em 30 min

            if ((moment().minute() % 5)==0) //verifica se a hora atual é arredondada
            {
                var hora = moment();// hora atual
                var horaPlus = moment().add(remainder30,"minutes");// hora atual + 30min
            }
            else{
                var hora = moment().add(remainder5,"minutes");// hora atual arredondada
                var horaPlus = moment().add(remainder30,"minutes");// hora atual arredondada + 30min
            }

        // :: Adiciona classe ui-state-active quando hora e minuto são clicados (timepicker style) ::
        $(".selectMinutes .ui-state-default").click(function () {
            $(".selectMinutes .ui-state-default").removeClass("ui-state-active");
            $(this).addClass("ui-state-active");
        });
        $(".selectHoras .ui-state-default").click(function () {
            $(".selectHoras .ui-state-default").removeClass("ui-state-active");
            $(this).addClass("ui-state-active");
        });

        // :: Botões de incrementos de minutos ::
        final = moment(horaPlus);

        window.incre15 = function () {
            final = moment(final).add(15, "minutes");
            $(".modal-body #id_horaFinal").val(moment(final).format("HH:mm"));// coloca data atual no input
        };

        window.incre30 = function () {
            final = moment(final).add(30, "minutes");
            $(".modal-body #id_horaFinal").val(moment(final).format("HH:mm"));// coloca data atual no input
        };

        window.incre60 = function () {
            final = moment(final).add(60, "minutes");
            $(".modal-body #id_horaFinal").val(moment(final).format("HH:mm"));// coloca data atual no input
        };

        // --> Limpando dados da hora final quando fecha modal
        $(document).on("click", ".btn-default", function () { // setando hora final para hora atual qnd modal fechar - btn cancel
            final = moment();
        });
        $(document).on("click", ".close", function () { // setando hora final para hora atual qnd modal fechar  - btn close
            final = moment();
        });

        // :: Passa valores para modal ::
        $(".modal-body #datepicker").val(moment(data).format("DD:MM:YYYY"));// coloca data atual no input
        $(".modal-body #id_horaInicial").val(moment(hora).format("HH:mm"));// coloca data atual no input
        $(".modal-body #id_horaFinal").val(moment(horaPlus).format("HH:mm"));// coloca data atual no input


     });


//Modal - Btn Plus*/
/*    $(document).on("click", ".plus", function () { // Função para agendar através do botão plus
        $('#reserva-currentDate').modal();// abre modal

        var data = moment().format("DD/MM/YYYY");// data atual
        remainder = 5 - (moment().minute() % 5);//arredonamento da hora atual. De 5 em 5 min
        if ((moment().minute() % 5)==0) // verifica se se a hora já está areedondada
        {
            var hora = moment().format("HH:mm");// hora atual
            var horaPlus = moment().add(30,"minutes").format("HH:mm");// hora atual + 30min
        }
        else{
            var hora = moment().add(remainder,"minutes").format("HH:mm");// hora atual arredondada
            var horaPlus = moment().add(remainder,"minutes").format("HH:mm");// hora atual arredondada + 30min
        }

        //  :: ATENÇÃO!! :: ARRUMAR ARREDONAMENTO - Quando add minutos ele está pegando a hora atual original e não a arredondada
        final = moment();

        $(document).on("click", ".btn-default", function () { // setando hora final para hora atual qnd modal fechar - btn cancel
            final = moment();
        });
        $(document).on("click", ".close", function () { // setando hora final para hora atual qnd modal fechar  - btn close
            final = moment();
        });

        window.add30 = function() { // Função para add 30 minutos
            final = moment(final).add(30, "minutes");
            document.getElementById('id_horaFinal').value = moment(final).format("HH:mm");
        }

        window.add10 = function() { // Função para add 10 minutos
            final = moment(final).add(10, "minutes");
            document.getElementById('id_horaFinal').value = moment(final).format("HH:mm");
        }

        window.add60 = function() { // Função para add 60 minutos
            final = moment(final).add(60, "minutes");
            document.getElementById('id_horaFinal').value = moment(final).format("HH:mm");
        }

        $(".modal-body #id_data").val(data);// coloca data atual no input
        $(".modal-body #id_horaInicial").val(hora);// coloca data atual no input
        $(".modal-body #id_horaFinal").val(horaPlus);// coloca data atual no input
        function myFunction(e) {
            document.getElementById("myText").value = e.target.value
        }
    });*/


/*//Modal - Btn Plus
    $(document).on("click", ".plus", function () { // Função para agendar através do botão plus
        $('#reserva-currentDate').modal();// abre modal
        var data = moment().format("DD/MM/YYYY");// data atual
        var hora = moment().format("HH:mm");// data atual
        $(".modal-body #id_data").val( data );// coloca data atual no input
        $(".modal-body #id_horaInicial").val( hora );// coloca data atual no input
        $(".modal-body #id_horaFinal").val( hora );// coloca data atual no input
    });*/
